import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OverSpeedRepoPage } from './over-speed-repo';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    OverSpeedRepoPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(OverSpeedRepoPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ],
})
export class OverSpeedRepoPageModule {}
