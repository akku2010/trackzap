import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddReminderPage } from './add-reminder';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddReminderPage,
  ],
  imports: [
    IonicPageModule.forChild(AddReminderPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class AddReminderPageModule {}
